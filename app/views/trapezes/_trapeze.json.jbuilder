json.extract! trapeze, :id, :side, :biggerBase, :smallerBase, :created_at, :updated_at
json.url trapeze_url(trapeze, format: :json)
