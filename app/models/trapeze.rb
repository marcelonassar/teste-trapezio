class Trapeze < ApplicationRecord
    validate :side_valid
    validate :bigger_side_valid
    validate :smaller_side_valid

    def area
        ((biggerBase + smallerBase) * side)/2
    end

    def perimeter
        side + smallerBase + biggerBase + Integer.sqrt(side**2 + (biggerBase - smallerBase)**2)
    end

    private 
    def side_valid
        if side
            errors.add(:side, 'Lado precisa ser maior que 0') if side <= 0
            errors.add(:side, 'Lado precisa ser menos que 50') if side >= 50
        else
            errors.add(:side, 'Lado precisa existir')
        end
    end

    def bigger_side_valid
        if biggerBase
            errors.add(:biggerBase, 'O lado maior precisa ser maior que 0') if biggerBase <= 0
        else
            errors.add(:biggerBase, 'O lado maior precisa existir')
        end
    end

    def smaller_side_valid
        if smallerBase
            errors.add(:smallerBase, 'O lado menor precisa ser meior que 0') if smallerBase <= 0
        else
            errors.add(:smallerBase, 'O lado menor precisa existir')
        end
    end
end
