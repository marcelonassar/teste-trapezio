require 'rails_helper'

RSpec.describe Trapeze, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
  it "create trapezes only with valid side sizes" do 
    Trapeze.create
    Trapeze.create(side: 5, biggerBase: 10, smallerBase: 6)
    Trapeze.create(side: 10)
    Trapeze.create(side: 10, biggerBase: 4, smallerBase: -5)
    Trapeze.create(side: 60, biggerBase: 10, smallerBase: 5)

    query = Trapeze.all

    expect(query.length).to eq(1)
  end

  it "calculates the right perimeter" do 
    trapeze = Trapeze.create(side: 5, biggerBase: 10, smallerBase: 5)
    expect(trapeze.perimeter).to eq(27.0)
    trapeze = Trapeze.create(side: 5, biggerBase: 15, smallerBase: 10)
    expect(trapeze.perimeter).to eq(37.0)
  end

  it "calculate the correct area" do 
    trapeze = Trapeze.create(side: 4, biggerBase: 10, smallerBase: 5)
    expect(trapeze.area).to eq(30.0)
    trapeze = Trapeze.create(side: 8, biggerBase: 4, smallerBase: 3)
    expect(trapeze.area).to eq(28.0)
  end
end
