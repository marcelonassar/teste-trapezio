require 'rails_helper'

RSpec.describe "trapezes/new", type: :view do
  before(:each) do
    assign(:trapeze, Trapeze.new(
      :side => 1,
      :biggerBase => 1,
      :smallerBase => 1
    ))
  end

  it "renders new trapeze form" do
    render

    assert_select "form[action=?][method=?]", trapezes_path, "post" do

      assert_select "input[name=?]", "trapeze[side]"

      assert_select "input[name=?]", "trapeze[biggerBase]"

      assert_select "input[name=?]", "trapeze[smallerBase]"
    end
  end
end
