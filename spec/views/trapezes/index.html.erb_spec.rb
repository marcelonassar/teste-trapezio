require 'rails_helper'

RSpec.describe "trapezes/index", type: :view do
  before(:each) do
    assign(:trapezes, [
      Trapeze.create!(
        :side => 2,
        :biggerBase => 3,
        :smallerBase => 4
      ),
      Trapeze.create!(
        :side => 2,
        :biggerBase => 3,
        :smallerBase => 4
      )
    ])
  end

  it "renders a list of trapezes" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
  end
end
