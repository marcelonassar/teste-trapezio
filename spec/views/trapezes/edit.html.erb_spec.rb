require 'rails_helper'

RSpec.describe "trapezes/edit", type: :view do
  before(:each) do
    @trapeze = assign(:trapeze, Trapeze.create!(
      :side => 1,
      :biggerBase => 1,
      :smallerBase => 1
    ))
  end

  it "renders the edit trapeze form" do
    render

    assert_select "form[action=?][method=?]", trapeze_path(@trapeze), "post" do

      assert_select "input[name=?]", "trapeze[side]"

      assert_select "input[name=?]", "trapeze[biggerBase]"

      assert_select "input[name=?]", "trapeze[smallerBase]"
    end
  end
end
