class CreateTrapezes < ActiveRecord::Migration[5.2]
  def change
    create_table :trapezes do |t|
      t.integer :side
      t.integer :biggerBase
      t.integer :smallerBase

      t.timestamps
    end
  end
end
